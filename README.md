# BiliCC-Srt

#### 介绍
用于下载BiliBili CC字幕的工具。
用QT5制作，编译发布有Windows及MacOS，另有Python版。
（MacOS是虚拟机下做的，真苹果没条件测试，若有使用苹果的，希望能给点反馈）

#### 小说明
-  **字幕路径** ：Win和Python版字幕文件位于软件根目录（软件所在的位置），MacOS在下载目录。
-  **查看BV号** ：URL www.bilibili.com/video/BV1w4411P7xn?from=search 中，BV号为1w4411P7xn或者BV1w4411P7xn（都可）。
 
-  **Python版使用** ：一、安装Python3环境，二、安装Requests，三、执行BILIC.py。
 
-  **没有文件** ：应该是所在目录不够权限读写，请尝试以管理员身份运行或换目录。
-  **乱码问题** ：似乎只是暴风影音对UTF-8支持不太好，若有需要，可以尝试使用GB2312编码。
-  **无法启动** ：请安装"Microsoft Visual C++ 2015-2019 Redistributable"(百度“微软运行库合集”下载安装亦可)
 
- 再附个测试例子：13t411U7Vm
 
#### 蓝奏云
https://kgd.lanzoux.com/b00naiahe 密码:ahxs